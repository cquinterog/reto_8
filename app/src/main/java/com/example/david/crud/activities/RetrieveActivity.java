package com.example.david.crud.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.david.crud.R;
import com.example.david.crud.dao.Company;
import com.example.david.crud.dao.SQLiteDAO;

import java.util.ArrayList;
import java.util.List;

public class RetrieveActivity extends AppCompatActivity {

    private ListView companiesListView;
    private ArrayAdapter<String> companiesListAdapter;
    private SQLiteDAO sqLiteDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrieve);

        companiesListView = (ListView) findViewById(R.id.companies_list_view);

        companiesListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        companiesListView.setAdapter(companiesListAdapter);
        sqLiteDAO = new SQLiteDAO(this);

        showCompanies();

    }

    private void showCompanies() {
        sqLiteDAO.open();

        List<Company> companies = sqLiteDAO.getAllCompanies(null, null);

        for (Company company : companies) {
            companiesListAdapter.add(company.toString());
        }

        sqLiteDAO.close();
    }
}
