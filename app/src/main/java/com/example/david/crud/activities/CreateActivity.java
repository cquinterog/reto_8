package com.example.david.crud.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.crud.R;
import com.example.david.crud.dao.Company;
import com.example.david.crud.dao.SQLiteDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateActivity extends AppCompatActivity {

    private Button addCompanyButton;
    private Button addProductButton;

    private EditText classificationEditText;

    private ListView productsListView;

    private Map<Integer, EditText> fields;
    private Map<Integer, CheckBox> classificationBoxes;

    private ArrayAdapter<String> productAdapter;

    private SQLiteDAO sqLiteDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        sqLiteDAO = new SQLiteDAO(this);

        addCompanyButton = (Button) findViewById(R.id.create_company_button);
        addProductButton = (Button) findViewById(R.id.add_class_button);
        classificationEditText = (EditText) findViewById(R.id.classification_edit_text);
        productsListView = (ListView) findViewById(R.id.products_list_view);

        fields = new HashMap<>();
        classificationBoxes = new HashMap<>();

        productAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);

        fields.put(R.id.company_name_edit_text, (EditText) findViewById(R.id.company_name_edit_text));
        fields.put(R.id.company_email_edit_text, (EditText) findViewById(R.id.company_email_edit_text));
        fields.put(R.id.company_URL_edit_text, (EditText) findViewById(R.id.company_URL_edit_text));
        fields.put(R.id.company_phone_number_edit_text, (EditText) findViewById(R.id.company_phone_number_edit_text));

        classificationBoxes.put(R.id.factory_check_box, (CheckBox) findViewById(R.id.factory_check_box));
        classificationBoxes.put(R.id.development_check_box, (CheckBox) findViewById(R.id.development_check_box));
        classificationBoxes.put(R.id.consultancy_check_box, (CheckBox) findViewById(R.id.consultancy_check_box));

        productsListView.setAdapter(productAdapter);

        addCompanyButton.setOnClickListener(new AddCompanyOnClickListener());
        addProductButton.setOnClickListener(new AddProductOnClickListener());

    }

    class AddCompanyOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            sqLiteDAO.open();
            String name = fields.get(R.id.company_name_edit_text).getText().toString();
            String email = fields.get(R.id.company_email_edit_text).getText().toString();
            String url = fields.get(R.id.company_URL_edit_text).getText().toString();
            String phone = fields.get(R.id.company_phone_number_edit_text).getText().toString();

            if (name.equals("") || email.equals("") || url.equals("") || phone.equals("")) {
                Toast.makeText(CreateActivity.this, R.string.no_fields, Toast.LENGTH_SHORT).show();
                return;
            }

            Company company = new Company();
            company.setName(name);
            company.setEmail(email);
            company.setPhone(phone);
            company.setUrl(url);

            sqLiteDAO.addCompany(company);

            for (Map.Entry<Integer, CheckBox> entry : classificationBoxes.entrySet()) {
                if (entry.getValue().isChecked()) {
                    String text = "";
                    switch (entry.getKey()) {
                        case R.id.factory_check_box:
                            text = "factory_text";
                            break;
                        case R.id.development_check_box:
                            text = "development_text";
                            break;
                        case R.id.consultancy_check_box:
                            text = "consultancy_text";
                            break;
                    }
                    sqLiteDAO.addClassification(text, company);
                }
            }

            for (int i = 0; i < productsListView.getCount(); ++i) {
                String text = (String) productsListView.getItemAtPosition(i);
                sqLiteDAO.addProduct(text, company);
            }

            sqLiteDAO.close();
            Toast.makeText(CreateActivity.this, R.string.success_message_create, Toast.LENGTH_SHORT).show();
        }
    }

    class AddProductOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String text = classificationEditText.getText().toString();
            if (text.equals("")) {
                Toast.makeText(CreateActivity.this, R.string.no_fields, Toast.LENGTH_SHORT).show();
                return;
            }
            productAdapter.add(text);
            classificationEditText.setText("");
        }
    }
}
