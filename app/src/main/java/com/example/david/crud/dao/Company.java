package com.example.david.crud.dao;

/**
 * Created by david on 13/11/17.
 */

public class Company {
    private long companyId;
    private String name;
    private String url;
    private String phone;
    private String email;

    public Company(long companyId, String name, String url, String phone, String email, String dept){
        this.companyId = companyId;
        this.name = name;
        this.url = url;
        this.phone = phone;
        this.email = email;
    }

    public Company(){

    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString(){
        return "Company ID: " + getCompanyId()+ "\n" +
                "Name: " + getName() + "\n" +
                "URL" + getUrl() + "\n" +
                "Email: "+ getEmail();
    }
}
