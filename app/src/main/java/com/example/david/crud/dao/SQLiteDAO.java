package com.example.david.crud.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by david on 13/11/17.
 */

public class SQLiteDAO {
    public static final String LOG_TAG = "CPY_MNGMNT_SYS";

    SQLiteOpenHelper dbHandler;
    SQLiteDatabase database;

    private static final String[] companiesAllColumns = {
            DBHandler.COLUMN_COMPANIES_ID,
            DBHandler.COLUMN_COMPANIES_NAME,
            DBHandler.COLUMN_COMPANIES_EMAIL,
            DBHandler.COLUMN_COMPANIES_PHONE,
            DBHandler.COLUMN_COMPANIES_URL,
    };

    public SQLiteDAO(Context context){
        dbHandler = new DBHandler(context);
    }

    public void open(){
        Log.i(LOG_TAG, "Database Opened");
        database = dbHandler.getWritableDatabase();
    }
    public void close(){
        Log.i(LOG_TAG, "Database Closed");
        dbHandler.close();
        database = null;
    }

    public long addCompany(Company company) {
        ContentValues values  = new ContentValues();
        values.put(DBHandler.COLUMN_COMPANIES_NAME, company.getName());
        values.put(DBHandler.COLUMN_COMPANIES_EMAIL, company.getUrl());
        values.put(DBHandler.COLUMN_COMPANIES_PHONE, company.getPhone());
        values.put(DBHandler.COLUMN_COMPANIES_URL, company.getEmail());
        long insertId = database.insert(DBHandler.TABLE_COMPANIES, null, values);
        company.setCompanyId(insertId);
        return insertId;
    }

    public long addProduct(String product, Company company) {
        ContentValues values  = new ContentValues();
        values.put(DBHandler.COLUMN_PRODUCTS_NAME, product);
        values.put(DBHandler.COLUMN_COMPANIES_ID, company.getCompanyId());
        long insertId = database.insert(DBHandler.TABLE_PRODUCTS, null, values);
        return insertId;
    }

    public long addClassification(String classification, Company company) {
        ContentValues values  = new ContentValues();
        values.put(DBHandler.COLUMN_CLASS_NAME, classification);
        values.put(DBHandler.COLUMN_COMPANIES_ID, company.getCompanyId());
        long insertId = database.insert(DBHandler.TABLE_CLASS, null, values);
        return insertId;
    }

    // Getting single Company
    public Company getCompany(long id) {
        Cursor cursor = database.query(
                DBHandler.TABLE_COMPANIES,
                companiesAllColumns,
                DBHandler.COLUMN_COMPANIES_ID + "=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null,
                null
        );
        if (cursor != null)
            cursor.moveToFirst();

        Company company = new Company(
                Long.parseLong(cursor.getString(0)),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5)
        );
        // return Company
        return company;
    }

    public List<Company> getAllCompanies(@Nullable String name[], @Nullable String classification[]) {

        /*
        Cursor cursor = database.rawQuery(
                "SELECT " + EMP_ID + ", " + NAME + ", " + DEPT + " FROM " + COMPANY + " JOIN " + DEPARTMENT +
                " ON " + COMPANY + "." + ID = DEPARTMENT.EMP_ID")
*/
        String where = null;

        if (name != null) {
            where = DBHandler.COLUMN_COMPANIES_NAME + "=?";
        }

        Cursor cursor = database.query(
                DBHandler.TABLE_COMPANIES,
                companiesAllColumns,
                where,
                name,
                null,
                null,
                DBHandler.COLUMN_COMPANIES_NAME
        );

        where = null;

        if (classification != null) {
            where = DBHandler.COLUMN_CLASS_NAME + "=?";
        }

        Cursor classCursor = database.query(
                DBHandler.TABLE_CLASS,
                new String[]{DBHandler.COLUMN_COMPANIES_ID},
                where,
                classification,
                DBHandler.COLUMN_COMPANIES_ID,
                null,
                null
        );

        Set<Long> idsAllowed = new HashSet<>();

        if (classCursor.getCount() > 0) {
            while (classCursor.moveToNext()) {
                idsAllowed.add(classCursor.getLong(classCursor.getColumnIndex(DBHandler.COLUMN_COMPANIES_ID)));
            }
        }

        List<Company> companies = new ArrayList<>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Company company = new Company();
                company.setCompanyId(cursor.getLong(cursor.getColumnIndex(DBHandler.COLUMN_COMPANIES_ID)));
                company.setName(cursor.getString(cursor.getColumnIndex(DBHandler.COLUMN_COMPANIES_NAME)));
                company.setUrl(cursor.getString(cursor.getColumnIndex(DBHandler.COLUMN_COMPANIES_EMAIL)));
                company.setPhone(cursor.getString(cursor.getColumnIndex(DBHandler.COLUMN_COMPANIES_PHONE)));
                company.setEmail(cursor.getString(cursor.getColumnIndex(DBHandler.COLUMN_COMPANIES_URL)));

                if (idsAllowed.contains(company.getCompanyId()) || classification == null) {
                    companies.add(company);
                }
            }
        }
        // return All Employees
        return companies;
    }

    public List<String> getProducts(Company company) {
        Cursor cursor = database.query(
                DBHandler.TABLE_PRODUCTS,
                new String[] {DBHandler.COLUMN_PRODUCTS_NAME},
                DBHandler.COLUMN_COMPANIES_ID + "=?",
                new String[]{String.valueOf(company.getCompanyId())},
                null,
                null,
                null,
                null
        );

        List<String> products = new ArrayList<>();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                products.add(
                        cursor.getString(
                                cursor.getColumnIndex(DBHandler.COLUMN_PRODUCTS_NAME)
                        )
                );
            }
        }
        // return Company
        return products;
    }

    public List<String> getClassifications(Company company) {
        Cursor cursor = database.query(
                DBHandler.TABLE_CLASS,
                new String[] {DBHandler.COLUMN_CLASS_NAME},
                DBHandler.COLUMN_COMPANIES_ID + "=?",
                new String[]{String.valueOf(company.getCompanyId())},
                null,
                null,
                null,
                null
        );

        List<String> classifications = new ArrayList<>();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                classifications.add(
                        cursor.getString(
                                cursor.getColumnIndex(DBHandler.COLUMN_CLASS_NAME)
                        )
                );
            }
        }
        // return Company
        return classifications;
    }

    // Updating Company
    public int updateCompany(Company company) {

        ContentValues values = new ContentValues();
        values.put(DBHandler.COLUMN_COMPANIES_NAME, company.getName());
        values.put(DBHandler.COLUMN_COMPANIES_EMAIL, company.getUrl());
        values.put(DBHandler.COLUMN_COMPANIES_PHONE, company.getPhone());
        values.put(DBHandler.COLUMN_COMPANIES_URL, company.getEmail());

        // updating row
        return database.update(DBHandler.TABLE_COMPANIES, values,
                DBHandler.COLUMN_COMPANIES_ID + "=?",new String[] { String.valueOf(company.getCompanyId())});
    }

    // Deleting Company
    public int removeCompany(Company company) {
        return database.delete(
                DBHandler.TABLE_COMPANIES, DBHandler.COLUMN_COMPANIES_ID + "=?",
                new String[]{
                        String.valueOf(company.getCompanyId())
                }
        );
    }

}
