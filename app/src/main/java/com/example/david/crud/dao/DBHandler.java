package com.example.david.crud.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by david on 13/11/17.
 */

public class DBHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "companies.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_COMPANIES = "companies";
    public static final String COLUMN_COMPANIES_ID = "companyId";
    public static final String COLUMN_COMPANIES_NAME = "name";
    public static final String COLUMN_COMPANIES_EMAIL = "email";
    public static final String COLUMN_COMPANIES_PHONE = "phone";
    public static final String COLUMN_COMPANIES_URL = "url";

    public static final String TABLE_PRODUCTS = "products";
    public static final String COLUMN_PRODUCTS_ID = "productId";
    public static final String COLUMN_PRODUCTS_NAME = "name";

    public static final String TABLE_CLASS = "classifications";
    public static final String COLUMN_CLASS_ID = "classId";
    public static final String COLUMN_CLASS_NAME = "name";

    private static final String TABLE_COMPANIES_CREATE =
            "CREATE TABLE " + TABLE_COMPANIES + " (" +
                    COLUMN_COMPANIES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_COMPANIES_NAME + " TEXT, " +
                    COLUMN_COMPANIES_EMAIL + " TEXT, " +
                    COLUMN_COMPANIES_PHONE + " TEXT, " +
                    COLUMN_COMPANIES_URL + " TEXT " +
                    ")";

    private static final String TABLE_PRODUCTS_CREATE =
            "CREATE TABLE " + TABLE_PRODUCTS + " (" +
                    COLUMN_PRODUCTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_COMPANIES_ID + " INTEGER, " +
                    COLUMN_PRODUCTS_NAME + " TEXT, " +
                    "FOREIGN KEY(" + COLUMN_COMPANIES_ID + ") REFERENCES " + TABLE_COMPANIES + "(" + COLUMN_COMPANIES_ID + ")" +
                    ")";

    private static final String TABLE_CLASS_CREATE =
            "CREATE TABLE " + TABLE_CLASS + " (" +
                    COLUMN_CLASS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_COMPANIES_ID + " INTEGER, " +
                    COLUMN_CLASS_NAME + " TEXT, " +
                    "FOREIGN KEY(" + COLUMN_COMPANIES_ID + ") REFERENCES " + TABLE_COMPANIES + "(" + COLUMN_COMPANIES_ID + ")" +
                    ")";

    public DBHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_COMPANIES_CREATE);
        db.execSQL(TABLE_PRODUCTS_CREATE);
        db.execSQL(TABLE_CLASS_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPANIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLASS);

        db.execSQL(TABLE_COMPANIES_CREATE);
        db.execSQL(TABLE_PRODUCTS_CREATE);
        db.execSQL(TABLE_CLASS_CREATE);
    }
}
