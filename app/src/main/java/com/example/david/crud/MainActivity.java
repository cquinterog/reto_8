package com.example.david.crud;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.david.crud.activities.CreateActivity;
import com.example.david.crud.activities.DeleteActivity;
import com.example.david.crud.activities.RetrieveActivity;
import com.example.david.crud.activities.UpdateActivity;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Map<Integer, Button> mainButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainButtons = new HashMap<>();

        mainButtons.put(R.id.create_button, (Button) findViewById(R.id.create_button));
        mainButtons.put(R.id.retrieve_button, (Button) findViewById(R.id.retrieve_button));
        mainButtons.put(R.id.update_button, (Button) findViewById(R.id.update_button));
        mainButtons.put(R.id.delete_button, (Button) findViewById(R.id.delete_button));

        MainButtonClickListener mainButtonClickListener = new MainButtonClickListener();

        for (Button button : mainButtons.values()) {
            button.setOnClickListener(mainButtonClickListener);
        }

    }

    class MainButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            final int id = v.getId();
            Intent intent = null;
            switch (id) {
                case R.id.create_button:
                    intent = new Intent(MainActivity.this, CreateActivity.class);
                    break;
                case R.id.retrieve_button:
                    intent = new Intent(MainActivity.this, RetrieveActivity.class);
                    break;
                case R.id.update_button:
                    intent = new Intent(MainActivity.this, UpdateActivity.class);
                    break;
                case R.id.delete_button:
                    intent = new Intent(MainActivity.this, DeleteActivity.class);
                    break;
            }
            if (intent == null) {
                Toast.makeText(MainActivity.this, R.string.error_message, Toast.LENGTH_SHORT);
                return;
            }
            startActivity(intent);
        }
    }
}
